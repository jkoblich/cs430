public class Jobs{
	String type;
	int startTime;
	int endTime;
	String prev;
	String mach;
	
	public Jobs(){
		type = "Idle";
		startTime = 0;
		endTime = 24;
		prev = "NONE";
		mach = "NONE";
	}
	
	public Jobs(String t, int s, int e, String p, String m){
		type = t;
		startTime = s;
		endTime = e;
		prev = p;
		mach = m;
	}
	
	public String getType(){
		return type;
	}
	
	public void setType(String ty){
		type = ty;
	}
	
	public int getStart(){
		return startTime;
	}
	
	public void setStart(int st){
		startTime = st;
	}
	
	public int getEnd(){
		return endTime;
	}
	
	public void setEnd(int en){
		endTime = en;
	}
	
	public String getP(){
		return prev;
	}
	
	public void setP(String p){
		prev = p;
	}
	
	public String getMach(){
		return mach;
	}
	
	public void setMach(String m){
		mach = m;
	}
	
	public String toString(){
		String output = "The job " + type + " starts at " + startTime + " and ends at " + endTime;
		return output;
	}
}
//