import java.io.*;
import java.util.*;
public class Schedule{
	public static void main(String[] args){
		ArrayList<Jobs> jobList = new ArrayList<Jobs>();				//Array List of all jobs
		ArrayList<Machines> machineList = new ArrayList<Machines>();	//Array List of all machines
		try{
			File file1 = new File("Machines.txt");						//imports list of machines
			File file2 = new File("Jobs.txt");							//imports list of jobs
			Scanner scan = new Scanner(file2);
			Scanner scan2 = new Scanner(file1);
			String jobName;
			int jobStart;
			int jobEnd;
			String machineName;
			while(scan2.hasNextLine()){									//scans machine txt file for names of machines
				String line = scan2.nextLine();
				StringTokenizer str = new StringTokenizer(line, " ");
				String m = str.nextToken();
				machineName = str.nextToken();
				Machines m1 = new Machines(machineName);
				machineList.add(m1);									//all machines stored in an array list
			}
			while(scan.hasNextLine()){
				String line = scan.nextLine();
				StringTokenizer str = new StringTokenizer(line, ",");	//scans job txt file for types of jobs and their start/end times
				jobName = str.nextToken();
				String a = str.nextToken();
				StringTokenizer str1 = new StringTokenizer(a, "-");
				jobStart = Integer.parseInt(str1.nextToken());
				jobEnd = Integer.parseInt(str1.nextToken());
				Jobs j1 = new Jobs(jobName, jobStart, jobEnd, "NONE", "NONE");
				jobList.add(j1);										//all jobs stored in an array List
			}
		}
		catch(FileNotFoundException fnf){ 
			System.out.println("Something is wrong with the text file");
		}
		
		createSchedule(jobList, machineList);
		
		for(int m=0; m < machineList.size(); m++){															//Outputs the schedule
			System.out.println("Machine " + machineList.get(m).getName() + " has the job(s):");
			for(int n=0; n <jobList.size(); n++){
				if(jobList.get(n).getMach() == machineList.get(m).getName()){
					System.out.println(jobList.get(n).getType() + ", from " + jobList.get(n).getStart() + " to " + jobList.get(n).getEnd());
				}
			}
			System.out.println("---------------------------------");
		}
		for(int n=0; n <jobList.size(); n++){																//Indicate which jobs were unable to be scheduled, if any.
			if(jobList.get(n).getMach() == "NONE"){
				System.out.println("\nThe job " + jobList.get(n).getType() + " was not assigned to a machine.");
			}
		}
	}
	public static void createSchedule(ArrayList<Jobs> jlist, ArrayList<Machines> mlist){     //The Brute Force Algorithm used to solve the problem
		
		selectionSort(jlist);	//Sort jobs by finish times so that f1 <= f2 <= ... <= fn.
		
		computeP(jlist);	//Compute p(1), p(2), …, p(n)   p(j) = largest job i < j such that job i is compatible with j.
		
		assignMachines(jlist, mlist);   //Assigns each grouping of jobs to a machine.
	}
	
	public static ArrayList<Jobs> selectionSort(ArrayList<Jobs> arr){ //sorts jobs in increasing order of end time --- Has O(n^2) time complexity
		for(int k=0; k<arr.size()-1; k++){
			int minIndex = k;
			for(int j=k+1; j<arr.size(); j++){
				if(arr.get(j).getEnd() < arr.get(minIndex).getEnd()){
					minIndex = j;
				}
			}
			Jobs temp = arr.get(k);
			arr.set(k, arr.get(minIndex));
			arr.set(minIndex, temp);
		}
		return arr;
	}
	
	public static ArrayList<Jobs> computeP(ArrayList<Jobs> j){	//Finds job that can be scheduled before it without overlap --- Has O(n^2) time complexity
		for(int i = j.size()-1; i >= 0; i--){
			for(int k = j.size()-1; k >= 0; k--){
				if(j.get(i).getStart() >= j.get(k).getEnd()){  //if job i starts after or when job k ends, and job i does not have a previous, set job k to be job i's previous
					if(j.get(i).getP() == "NONE"){//
						j.get(i).setP(j.get(k).getType());
						k=0;
					}
				}
			}
		}
		return j;
	}
	
	public static ArrayList<Jobs> assignMachines(ArrayList<Jobs> j, ArrayList<Machines> m){	//Assigns each grouping of jobs to a machine. --- Has O(n^2) time complexity
		int k=0;
		for(int i = j.size()-1; i >= 0; i--){
			if(j.get(i).getP() != "NONE" && j.get(i).getMach() == "NONE" && k <= m.size()-1){	//if a job has a previous job and does not have a machine, assign it to machine k
				j.get(i).setMach(m.get(k).getName());
				for(int n = j.size()-1; n >= 0; n--){				
					if(j.get(n).getType() == (j.get(i).getP())){	//if a job IS a previous job, assign it to the same machine that it is a previous job to
						j.get(n).setMach(m.get(k).getName());
					}					
				}
				k++;
			}
			else if(j.get(i).getP() == "NONE" && j.get(i).getMach() == "NONE" && k <= m.size()-1){		//if a job does not have a previous job and does not have a machine, assign it to machine k
				j.get(i).setMach(m.get(k).getName());
				k++;
			}
		}
		return j;
	}
}