public class Machines{
	String name;
	
	public Machines(){
		name = "Unknown";
	}
	
	public Machines(String n){
		name = n;
	}
	
	public String getName(){
		return name;
	}
	
	public void setName(String nam){
		name = nam;
	}
	
	public String toString(){
		String output = "Machine: " + name;
		return output;
	}
}
//