1. Make sure that all of the following files are in the same directory(5 files):
	Schedule.java  (The client / main program)
	Jobs.java
	Machines.java
	Jobs.txt		(Input file)
	Machines.txt	(Input file)
	
2.Compiling the files
	
	On Windows/Mac/Linux with Java installed:
		Open command prompt, cd into the directory that all files are in
		Type each of these lines separately:
			javac Jobs.java
			javac Machines.java
			javac Schedule.java
		There should now be 3 new files(Jobs.class, Machines.class, Schedule.class)
		Everything is ready. To run the program, type:
			java Schedule
		The output should be visible.
		
3. Changing the input file

	Jobs.txt is the file that holds the list of jobs
		Each line represents only one job in the form: [NameOfJob],[StartingTime]-[EndingTime]
		There are no spaces or any other characters.
		Times are in 24hr/military time: 1 = 1am, 12 = 12pm, 13 = 1pm, 24 = 12am  (1,2,3,...23,24,1,2,3,...)
		Duration of jobs are whole numbers (ex: not 2.75 hours long)
		Jobs start and end on whole hours (ex: 4-6, not 4:15-6:15)
		Example of a valid job: Print,14-16
	Machines.txt is the file that represents the list of machines
		Each line represents only one machine in the form: Machine [NameOfMachine]
		The only space is between the word 'Machine' and the name of the machine
		Example of a valid machine: Machine Laptop
	Do not change the names of these two files.
	Do not add any other indents, spaces, blank lines, etc.